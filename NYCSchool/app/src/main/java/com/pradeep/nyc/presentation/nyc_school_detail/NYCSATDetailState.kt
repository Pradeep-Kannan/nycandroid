package com.pradeep.nyc.presentation.nyc_school_detail

import com.pradeep.nyc.data.remote.dto.NYCDetailItem

/**
 * data class for NYC SAT Detail Screen
 */
data class NYCSATDetailState(
    val isLoading: Boolean = false,
    val nycsatDetail: List<NYCDetailItem>? = null,
    val error: String = ""
)
