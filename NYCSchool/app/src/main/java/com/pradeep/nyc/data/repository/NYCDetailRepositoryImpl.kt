package com.pradeep.nyc.data.repository

import com.pradeep.nyc.data.remote.NYCApi
import com.pradeep.nyc.data.remote.dto.NYCDetailItem
import com.pradeep.nyc.domain.repository.NYCDetailRepository
import javax.inject.Inject

/**
 * repository impl class to fetch SAT Score
 */
class NYCDetailRepositoryImpl @Inject constructor(
    private val api: NYCApi
) : NYCDetailRepository {

    override suspend fun getNYCDetailWithDBNId(dbnId: String): List<NYCDetailItem> {
        return api.getNYCDetail(dbn = dbnId)
    }
}