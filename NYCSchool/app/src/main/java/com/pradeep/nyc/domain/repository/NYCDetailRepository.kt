package com.pradeep.nyc.domain.repository

import com.pradeep.nyc.data.remote.dto.NYCDetailItem

interface NYCDetailRepository {
    suspend fun getNYCDetailWithDBNId(dbnId:String): List<NYCDetailItem>
}