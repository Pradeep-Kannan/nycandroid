package com.pradeep.nyc.presentation

sealed class Screen(val route: String) {
    object NYCSchoolListScreen: Screen("nyc_school_list_screen")
    object NYCSchoolDetailScreen: Screen("nyc_school_detail_screen")
}
