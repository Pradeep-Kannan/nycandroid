package com.pradeep.nyc.di

import com.pradeep.nyc.BuildConfig
import com.pradeep.nyc.data.remote.NYCApi
import com.pradeep.nyc.data.repository.NYCDetailRepositoryImpl
import com.pradeep.nyc.data.repository.NYCRepositoryImpl
import com.pradeep.nyc.domain.repository.NYCDetailRepository
import com.pradeep.nyc.domain.repository.NYCRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * create DI for all objects using below class
 */
@Module
@InstallIn(SingletonComponent::class)
object NYCAppModule {
    /**
     * To debug the RETROFIT calls use the below method
     */
    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient : OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
        return httpClient
    }

    /**
     * This class provide the retrofit object.
     * Enable the httpclient, we can debug the retrofit calls.
     */
    @Provides
    @Singleton
    fun provideNYCApi(httpClient: OkHttpClient.Builder): NYCApi {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            //.client(httpClient.build())
            .build()
            .create(NYCApi::class.java)
    }

    /**
     * DI for NYCRepository
     */
    @Provides
    @Singleton
    fun provideNYCRepository(api:NYCApi): NYCRepository {
        return NYCRepositoryImpl(api)
    }

    /**
     * DI for NYCDetailRepository
     */
    @Provides
    @Singleton
    fun provideNYCDetailRepository(api:NYCApi): NYCDetailRepository {
        return NYCDetailRepositoryImpl(api)
    }

}