package com.pradeep.nyc.domain.usecase.get_nyc_detail

import com.pradeep.nyc.common.Resource
import com.pradeep.nyc.data.remote.dto.NYCDetailItem
import com.pradeep.nyc.domain.repository.NYCDetailRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

/**
 * To retrieve the SAT score, we need to send the dbn id.
 */
class GetNYCDetailUseCase @Inject constructor(
    private val repository: NYCDetailRepository,
) {
    operator fun invoke(dbnId: String): Flow<Resource<List<NYCDetailItem>>> = flow {
        try {
            emit(Resource.Loading<List<NYCDetailItem>>())
            val nycdata = repository.getNYCDetailWithDBNId(dbnId).map { it }
            emit(Resource.Success<List<NYCDetailItem>>(nycdata))
        } catch (e: HttpException) {
            emit(
                Resource.Error<List<NYCDetailItem>>(
                    e.localizedMessage ?: "An unexpected error occured"
                )
            )
        } catch (e: IOException) {
            emit(Resource.Error<List<NYCDetailItem>>("Couldn't reach server. Check your internet connection."))
        }
    }
}