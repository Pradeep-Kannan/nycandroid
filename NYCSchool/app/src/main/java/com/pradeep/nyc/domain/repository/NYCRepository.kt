package com.pradeep.nyc.domain.repository

import com.pradeep.nyc.data.remote.dto.NYCDataItem

interface NYCRepository {
    suspend fun getNYCData(limit:Int,offset: Int): List<NYCDataItem>
}