package com.pradeep.nyc.common

object Constants {
    //dbn ID to retrieve SAT Score
    const val PARAM_DBN_ID = "dbnId"
    // TAG to debug the app
    const val TAG = "AppDebug"
}