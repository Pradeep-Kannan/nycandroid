package com.pradeep.nyc.data.remote

import com.pradeep.nyc.BuildConfig
import com.pradeep.nyc.common.NetworkConstants.X_APP_TOKEN
import com.pradeep.nyc.data.remote.dto.NYCDataItem
import com.pradeep.nyc.data.remote.dto.NYCDetailItem
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

/**
 * class to hit api using retrofit
 */
interface NYCApi {
    @GET("/resource/s3k6-pzi2")
    suspend fun getNYCData(
        @Header(X_APP_TOKEN) token: String = BuildConfig.APP_TOKEN,
        @Query("\$limit") limit: Int,
        @Query("\$offset") offset: Int,
    ): List<NYCDataItem>

    @GET("/resource/f9bf-2cp4")
    suspend fun getNYCDetail(
        @Header(X_APP_TOKEN) token: String = BuildConfig.APP_TOKEN,
        @Query("dbn") dbn: String
    ): List<NYCDetailItem>
}