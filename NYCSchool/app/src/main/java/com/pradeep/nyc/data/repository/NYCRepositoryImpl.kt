package com.pradeep.nyc.data.repository

import com.pradeep.nyc.data.remote.NYCApi
import com.pradeep.nyc.data.remote.dto.NYCDataItem
import com.pradeep.nyc.domain.repository.NYCRepository
import javax.inject.Inject

/**
 * repository impl class to fetch all schools data
 */
class NYCRepositoryImpl @Inject constructor(
    private val api: NYCApi
) : NYCRepository {

    override suspend fun getNYCData(limit:Int, offset: Int): List<NYCDataItem> {
        return api.getNYCData(limit = limit, offset = offset)
    }
}