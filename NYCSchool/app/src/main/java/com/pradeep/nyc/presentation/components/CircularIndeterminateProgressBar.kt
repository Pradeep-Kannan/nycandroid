package com.pradeep.nyc.presentation.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

/**
 * Center a circular indeterminate progress bar.
 */
@Composable
fun CircularIndeterminateProgressBar(isDisplayed: Boolean) {
    if (isDisplayed) {
        Box(
            modifier = Modifier.fillMaxSize(),
        ) {
           Row(modifier = Modifier.align(Alignment.Center)) {
               CircularProgressIndicator(
                   modifier = Modifier,
                   color = MaterialTheme.colors.primary
               )
           }
        }
    }
}



















