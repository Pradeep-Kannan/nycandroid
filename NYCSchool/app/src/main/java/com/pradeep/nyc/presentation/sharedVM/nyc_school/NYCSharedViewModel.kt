package com.pradeep.nyc.presentation.sharedVM.nyc_school

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.pradeep.nyc.data.remote.dto.NYCDataItem

/**
 * Shared view model to share info across composables.
 */
class NYCSharedViewModel : ViewModel() {
    var nycDataItem by mutableStateOf<NYCDataItem?>(null)
        private set

    fun setNYCDataItem(nycDataItem: NYCDataItem?) {
        this.nycDataItem = nycDataItem
    }

}