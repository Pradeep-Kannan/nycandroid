package com.pradeep.nyc.data.remote.dto

/**
 * data class for NYC Detail item, especially for SAT Score
 */
data class NYCDetailItem(
    val dbn: String,
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String,
    val school_name: String
)