package com.pradeep.nyc.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.pradeep.nyc.common.Constants
import com.pradeep.nyc.presentation.sharedVM.nyc_school.NYCSharedViewModel
import com.pradeep.nyc.presentation.nyc_school_detail.NYCSchoolDetailScreen
import com.pradeep.nyc.presentation.nyc_school_list.NYCSchoolListScreen
import com.pradeep.nyc.presentation.ui.theme.NYCSchoolTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCSchoolTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    //Shared view model to share info between composables screen
                    val nycSharedViewModel: NYCSharedViewModel = viewModel()
                    //navcontroller to move destination between one screen to other
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.NYCSchoolListScreen.route
                    ) {
                        //List Screen
                        composable(
                            route = Screen.NYCSchoolListScreen.route
                        ) {
                            NYCSchoolListScreen(navController,sharedViewModel = nycSharedViewModel)
                        }
                        //Detail Screen, send dbnID to fetch SAT Score
                        composable(
                            route = Screen.NYCSchoolDetailScreen.route + "/{${Constants.PARAM_DBN_ID}}"
                        ) {
                            NYCSchoolDetailScreen(sharedViewModel = nycSharedViewModel)
                        }
                    }
                }
            }
        }
    }
}