package com.pradeep.nyc.presentation.nyc_school_detail

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.flowlayout.FlowRow
import com.pradeep.nyc.R
import com.pradeep.nyc.presentation.nyc_school_detail.components.NYCTag
import com.pradeep.nyc.presentation.sharedVM.nyc_school.NYCSharedViewModel
import com.pradeep.nyc.presentation.nyc_school_detail.components.SatScoreItem

/**
 * NYC School Detail Screen, we are displaying the
 * NYC Schoolname, overview, sat score, academic opportunities
 * extracurricular activities and contact us details which displays
 * location, phone number, fax, email and website.
 */
@Composable
fun NYCSchoolDetailScreen(
    viewModel: NYCSchoolDetailViewModel = hiltViewModel(),
    sharedViewModel: NYCSharedViewModel,
) {
    val state = viewModel.state.value
    val nycDataItem = sharedViewModel.nycDataItem
    Box(modifier = Modifier.fillMaxSize()) {
        nycDataItem?.let { nycItem ->
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                contentPadding = PaddingValues(20.dp)
            ) {
                item {
                    Row(
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(
                            text = nycItem.school_name,
                            style = MaterialTheme.typography.h5,
                        )
                    }
                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = nycItem.overview_paragraph,
                        style = MaterialTheme.typography.body2
                    )
                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = stringResource(R.string.sat_score),
                        style = MaterialTheme.typography.h5
                    )
                    state.nycsatDetail?.let { nycDetailList ->
                        if (nycDetailList.isNotEmpty())
                            SatScoreItem(nycDetailList[0])
                        else
                            Text(
                                text = stringResource(R.string.sat_not_found),
                                color = Color.Red,
                                style = MaterialTheme.typography.h6,
                                //fontStyle = FontStyle.Italic,
                                modifier = Modifier.padding(start = 20.dp)
                            )
                    } ?: run {
                        if (state.error.isNotBlank()) {
                            Text(
                                text = state.error,
                                color = Color.Red,
                                style = MaterialTheme.typography.body1,
                                modifier = Modifier.padding(start = 20.dp)
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = stringResource(R.string.academic_opp),
                        style = MaterialTheme.typography.h5
                    )
                    Spacer(modifier = Modifier.height(15.dp))
                    FlowRow(
                        mainAxisSpacing = 10.dp,
                        crossAxisSpacing = 10.dp,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        nycItem.academicopportunities1.split(",").let {
                            it.forEach { tag ->
                                NYCTag(tag = tag)
                            }
                        }
                    }
                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = stringResource(R.string.extra_curricular),
                        style = MaterialTheme.typography.h5
                    )
                    Spacer(modifier = Modifier.height(15.dp))
                    FlowRow(
                        mainAxisSpacing = 10.dp,
                        crossAxisSpacing = 10.dp,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        nycItem.extracurricular_activities.split(",").let {
                            it.forEach { tag ->
                                NYCTag(tag = tag)
                            }
                        }
                    }

                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = stringResource(R.string.contact_us),
                        style = MaterialTheme.typography.h5
                    )
                    Spacer(modifier = Modifier.height(15.dp))
                    Text(
                        text = nycItem.location,
                        modifier = Modifier.padding(start = 10.dp),
                        style = MaterialTheme.typography.body1,
                        fontStyle = FontStyle.Italic
                    )
                    Text(
                        text = "phone: ${nycItem.phone_number}",
                        modifier = Modifier.padding(start = 10.dp),
                        style = MaterialTheme.typography.body1,
                        fontStyle = FontStyle.Italic
                    )
                    Text(
                        text = "fax: ${nycItem.fax_number}",
                        modifier = Modifier.padding(start = 10.dp),
                        style = MaterialTheme.typography.body1,
                        fontStyle = FontStyle.Italic
                    )
                    Text(
                        text = "e-mail at: ${nycItem.school_email}",
                        modifier = Modifier.padding(start = 10.dp),
                        style = MaterialTheme.typography.body1,
                        fontStyle = FontStyle.Italic
                    )
                    Text(
                        text = "web: ${nycItem.website}",
                        modifier = Modifier.padding(start = 10.dp),
                        style = MaterialTheme.typography.body1,
                        fontStyle = FontStyle.Italic
                    )
                }
            }
        }
        if (state.isLoading) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
}