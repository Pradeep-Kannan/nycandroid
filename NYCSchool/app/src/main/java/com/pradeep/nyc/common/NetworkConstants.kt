package com.pradeep.nyc.common

object NetworkConstants {
    //Send app token in all request
    const val X_APP_TOKEN = "X-App-Token"
}