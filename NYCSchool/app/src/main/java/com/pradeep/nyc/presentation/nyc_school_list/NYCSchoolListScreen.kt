package com.pradeep.nyc.presentation.nyc_school_list

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.pradeep.nyc.presentation.components.util.SnackbarController
import com.pradeep.nyc.common.Constants
import com.pradeep.nyc.presentation.MainActivity
import com.pradeep.nyc.presentation.sharedVM.nyc_school.NYCSharedViewModel
import com.pradeep.nyc.presentation.Screen
import com.pradeep.nyc.presentation.components.DefaultSnackbar
import com.pradeep.nyc.presentation.nyc_school_list.components.NYCList
import kotlinx.coroutines.launch

/**
 * This is the homescreen which displays all the NYC Schools
 * By clicking anyone of the schools will go into detail screen
 * which displays the SAT Score and other information.
 * Launched effect is used to display the snackbar for the error scenarios.
 */
@Composable
fun NYCSchoolListScreen(
    navController: NavController,
    viewModel: NYCSchoolListViewModel = hiltViewModel(),
    sharedViewModel : NYCSharedViewModel
) {
    val loading = viewModel.loading.value
    val nycItems = viewModel.nycItems.value
    val page = viewModel.page.value
    val error = viewModel.error.value

    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    val snackBarController = SnackbarController(scope)
    if (error.isNotBlank()) {
        LaunchedEffect(key1 = error) {
            Log.d(Constants.TAG, "inside Launched!!!")
            snackBarController.getScope().launch {
                snackBarController.showSnackbar(
                    scaffoldState = scaffoldState,
                    message = error,
                    actionLabel = "Error"
                )
            }
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        NYCList(
            loading = loading,
            nycItems = nycItems,
            onChangeScrollPosition = viewModel::onChangeNYCScrollPosition,
            page = page,
            onTriggerNextPage = { viewModel.onTriggerEvent() },
            onNavigateToRecipeDetailScreen = {nycDataItem->
                Log.d(Constants.TAG, "DBN: ${nycDataItem.dbn}")
                sharedViewModel.setNYCDataItem(nycDataItem)
                navController.navigate(Screen.NYCSchoolDetailScreen.route + "/${nycDataItem.dbn}")
            }
        )
        if (error.isNotBlank()) {
            if(nycItems.isEmpty()) {
                Text(
                    text = error,
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp)
                        .align(Alignment.Center)
                )
            }else {
                DefaultSnackbar(
                    snackbarHostState = scaffoldState.snackbarHostState,
                    onDismiss = {
                        scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
                    },
                    modifier = Modifier.align(Alignment.BottomCenter)
                )
            }
        }
    }
}