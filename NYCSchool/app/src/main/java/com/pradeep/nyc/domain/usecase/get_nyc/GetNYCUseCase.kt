package com.pradeep.nyc.domain.usecase.get_nyc

import com.pradeep.nyc.common.Resource
import com.pradeep.nyc.data.remote.dto.NYCDataItem
import com.pradeep.nyc.domain.repository.NYCRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

/**
 * We are retrieving 30 records during every hit of the api.
 * send the limit and offset value to this api.
 */
class GetNYCUseCase @Inject constructor(
    private val repository: NYCRepository,
) {
    operator fun invoke(limit:Int, offset: Int): Flow<Resource<List<NYCDataItem>>> = flow {
        try {
            emit(Resource.Loading<List<NYCDataItem>>())
            val nycdata = repository.getNYCData(limit, offset).map { it }
            emit(Resource.Success<List<NYCDataItem>>(nycdata))
        } catch(e: HttpException) {
            emit(Resource.Error<List<NYCDataItem>>(e.localizedMessage ?: "An unexpected error occured"))
        } catch(e: IOException) {
            emit(Resource.Error<List<NYCDataItem>>("Couldn't reach server. Check your internet connection."))
        }
    }
}