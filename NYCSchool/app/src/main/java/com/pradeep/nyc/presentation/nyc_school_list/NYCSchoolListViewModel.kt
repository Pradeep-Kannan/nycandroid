package com.pradeep.nyc.presentation.nyc_school_list

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pradeep.nyc.common.Constants.TAG
import com.pradeep.nyc.common.Resource
import com.pradeep.nyc.data.remote.dto.NYCDataItem
import com.pradeep.nyc.domain.usecase.get_nyc.GetNYCUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

// Pagination size  30
const val PAGE_SIZE = 30

/**
 * Using GetNYCUseCase, get all the NYC Schools list
 */
@HiltViewModel
class NYCSchoolListViewModel @Inject constructor(
    private val getNYCUseCase: GetNYCUseCase
) : ViewModel() {
    // state object to store NYCDataItem
    val nycItems: MutableState<List<NYCDataItem>> = mutableStateOf(ArrayList())
    //Show progress bar based on this loading flag
    val loading = mutableStateOf(false)
    //Page info to achieve the pagination
    val page = mutableStateOf(1)
    //offset value  send to the request
    private val offset = mutableStateOf(0)
    //Show error message if any errors found in the backend calls
    val error = mutableStateOf("")

    //to determine scroll position,we are using this flag.
    private var nycListScrollPosition = 0

    init {
        getNYCSchoolListData()
    }

    /**
     * Retrieve all NYC Schools using the limit and offset values
     */
    private fun getNYCSchoolListData() {
        getNYCUseCase(PAGE_SIZE, offset.value).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    loading.value = false
                    error.value = ""
                    result.data?.let {
                        appendNYCItems(it)
                    }
                }
                is Resource.Error -> {
                    loading.value = false
                    error.value = result.message ?: "An unexpected error occured"
                }
                is Resource.Loading -> {
                    loading.value = true
                }
            }
        }.launchIn(viewModelScope)
    }

    /**
     * Trigger pagination when the user scrolls down to the last position.
     */
    fun onTriggerEvent(){
        viewModelScope.launch {
            try {
                nextPage()
            }catch (e: Exception){
                Log.e(TAG, "launchJob: Exception: ${e}, ${e.cause}")
                e.printStackTrace()
            }
        }
    }

    /**
     * call nextPage when the user scrolls down to the last position.
     */
    private fun nextPage(){
        // prevent duplicate event due to recompose happening to quickly
        if((nycListScrollPosition + 1) >= (page.value * PAGE_SIZE) ){
            incrementPage()
            getNYCSchoolListData()
        }
    }

    /**
     * increment page to 1
     */
    private fun incrementPage(){
        setPageAndOffset(page.value + 1)
    }

    /**
     * set page and offset value
     */
    private fun setPageAndOffset(page: Int){
        this.page.value = page
        this.offset.value += 30
    }

    /**
     * set the list scroll position while scrolling
     */
    fun onChangeNYCScrollPosition(position: Int){
        setListScrollPosition(position = position)
    }

    private fun setListScrollPosition(position: Int){
        nycListScrollPosition = position
    }

    /**
     * append all the NYC Schools to nycItems
     */
    private fun appendNYCItems(nycItems: List<NYCDataItem>){
        val current = ArrayList(this.nycItems.value)
        current.addAll(nycItems)
        this.nycItems.value = current
    }
}