package com.pradeep.nyc.presentation.nyc_school_detail

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pradeep.nyc.common.Constants
import com.pradeep.nyc.common.Resource
import com.pradeep.nyc.domain.usecase.get_nyc_detail.GetNYCDetailUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

/**
 * get DBN id from list screen and fetch the SAT Score
 * and display in this detail screen.
 */
@HiltViewModel
class NYCSchoolDetailViewModel @Inject constructor(
    private val getNYCDetailUseCase: GetNYCDetailUseCase,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    //mutable state object for NYCSATDetailState
    private val _state = mutableStateOf(NYCSATDetailState())
    val state: State<NYCSATDetailState> = _state

    init {
        savedStateHandle.get<String>(Constants.PARAM_DBN_ID)?.let { dbnID ->
            getNYCDetail(dbnID)
        }
    }

    /**
     * get sat score using dbnID.
     */
    private fun getNYCDetail(dbnID: String) {
        getNYCDetailUseCase(dbnID).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    Log.d(Constants.TAG, "result.data: ${result.data}")
                    _state.value = NYCSATDetailState(nycsatDetail = result.data)
                }
                is Resource.Error -> {
                    _state.value = NYCSATDetailState(
                        error = result.message ?: "An unexpected error occured"
                    )
                }
                is Resource.Loading -> {
                    _state.value = NYCSATDetailState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}