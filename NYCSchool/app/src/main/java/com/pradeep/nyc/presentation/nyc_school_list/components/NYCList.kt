package com.pradeep.nyc.presentation.nyc_school_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.pradeep.nyc.data.remote.dto.NYCDataItem
import com.pradeep.nyc.presentation.components.CircularIndeterminateProgressBar
import com.pradeep.nyc.presentation.nyc_school_list.PAGE_SIZE

/**
 * Lazy list for NYC schools which displays only School name.
 * Detail Screen contains other information.
 */
@Composable
fun NYCList(
    loading: Boolean,
    nycItems: List<NYCDataItem>,
    onChangeScrollPosition: (Int) -> Unit,
    page: Int,
    onTriggerNextPage: () -> Unit,
    onNavigateToRecipeDetailScreen: (NYCDataItem) -> Unit,
) {
    Box(
        modifier = Modifier
            .background(color = MaterialTheme.colors.surface)
    ) {
        CircularIndeterminateProgressBar(
            isDisplayed = loading
        )
        LazyColumn {
            itemsIndexed(
                items = nycItems
            ) { index, nycItem ->
                onChangeScrollPosition(index)
                if ((index + 1) >= (page * PAGE_SIZE) && !loading) {
                    onTriggerNextPage()
                }
                NYCListItem(
                    nycDataItem = nycItem,
                    onItemClick = {
                        onNavigateToRecipeDetailScreen(nycItem)
                    }
                )
            }
        }

    }
}