package com.pradeep.nyc.presentation.nyc_school_list.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.pradeep.nyc.data.remote.dto.NYCDataItem

/**
 * List item UI for NYC Schools list.
 */
@Composable
fun NYCListItem(
    nycDataItem: NYCDataItem,
    onItemClick: (NYCDataItem) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onItemClick(nycDataItem) }
            .padding(20.dp)
    ) {
        Text(
            text = nycDataItem.school_name,
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis
        )
    }
}