package com.pradeep.nyc.presentation.nyc_school_detail.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import com.pradeep.nyc.R
import com.pradeep.nyc.data.remote.dto.NYCDetailItem

/**
 * Layout for SAT Score display in Detail Screen.
 */
@Composable
fun SatScoreItem(
    nycDetailData: NYCDetailItem,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier
            .padding(10.dp)
            .border(
                border = BorderStroke(1.dp, Color.White),
                shape = RoundedCornerShape(20.dp)
            ),
        verticalArrangement = Arrangement.Center
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.no_of_students),
                style = MaterialTheme.typography.h6,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = nycDetailData.num_of_sat_test_takers,
                style = MaterialTheme.typography.h6,
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.read_avg_score),
                style = MaterialTheme.typography.h6,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = nycDetailData.sat_critical_reading_avg_score,
                style = MaterialTheme.typography.h6,
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.math_avg_score),
                style = MaterialTheme.typography.h6,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = nycDetailData.sat_math_avg_score,
                style = MaterialTheme.typography.h6,
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = stringResource(R.string.writing_avg_score),
                style = MaterialTheme.typography.h6,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = nycDetailData.sat_writing_avg_score,
                style = MaterialTheme.typography.h6,
            )
        }

    }
}